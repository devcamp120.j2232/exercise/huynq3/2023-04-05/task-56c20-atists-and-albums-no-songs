package com.devcamp.artistalbumapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.artistalbumapi.model.Album;
@Service
public class AlbumService {
        Album album1 = new Album(1,"001 album");
        Album album2 = new Album(2,"002 album");
        Album album3 = new Album(3,"003 album");
        Album album4 = new Album(4,"004 album");
        Album album5 = new Album(5,"005 album");
        Album album6 = new Album(6,"006 album");
    public ArrayList<Album> albumListAuthor1(){
        ArrayList<Album> author1Albums = new ArrayList<>();
        author1Albums.add(album1);
        author1Albums.add(album2);
        return author1Albums;
    }
    public ArrayList<Album> albumListAuthor2(){
        ArrayList<Album> author2Albums = new ArrayList<>();
        author2Albums.add(album3);
        author2Albums.add(album4);
        return author2Albums;
    }
    public ArrayList<Album> albumListAuthor3(){
        ArrayList<Album> author3Albums = new ArrayList<>();
        author3Albums.add(album5);
        author3Albums.add(album6);
        return author3Albums;
    }
    public ArrayList<Album> allAlbum(){
        ArrayList<Album> albumList = new ArrayList<>();
        albumList.add(album1);
        albumList.add(album2);
        albumList.add(album3);
        albumList.add(album4);
        albumList.add(album5);
        albumList.add(album6);
        return albumList;

    }
}
