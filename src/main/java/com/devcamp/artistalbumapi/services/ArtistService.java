package com.devcamp.artistalbumapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.artistalbumapi.model.Artist;
@Service
public class ArtistService extends AlbumService{
    public ArrayList<Artist> artistList(){
        ArrayList<Artist> artistList = new ArrayList<>();
        Artist artist1 = new Artist(101,"Huy Nguyen 18+", albumListAuthor1());
        Artist artist2 = new Artist(102,"Yen Vu Chirlden", albumListAuthor2());
        Artist artist3 = new Artist(103,"Son Vo ATM", albumListAuthor3());
        artistList.add(artist1);
        artistList.add(artist2);
        artistList.add(artist3);
        return artistList;
    }
}
